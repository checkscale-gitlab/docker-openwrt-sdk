FROM alpine:latest

LABEL \
maintainer="Marek Rychly <marek.rychly@gmail.com>"

ARG OPENWRT_ARCH
ARG OPENWRT_GCC
ARG OPENWRT_HOST
ARG OPENWRT_TARGET
ARG OPENWRT_SUBTARGET
ARG OPENWRT_VERSION

ENV \
OPENWRT_ARCH="${OPENWRT_ARCH:-mips_24kc}" \
OPENWRT_GCC="${OPENWRT_GCC:-8.4.0_musl}" \
OPENWRT_HOST="${OPENWRT_HOST:-Linux-x86_64}" \
OPENWRT_TARGET="${OPENWRT_TARGET:-ath79}" \
OPENWRT_SUBTARGET="${OPENWRT_SUBTARGET:-generic}" \
OPENWRT_VERSION="${OPENWRT_VERSION:-21.02.0}"

ENV \
SDK_NAME="openwrt-sdk-${OPENWRT_VERSION}-${OPENWRT_TARGET}-${OPENWRT_SUBTARGET}_gcc-${OPENWRT_GCC}.${OPENWRT_HOST}"

ENV \
SDK_URL="https://downloads.openwrt.org/releases/${OPENWRT_VERSION}/targets/${OPENWRT_TARGET}/${OPENWRT_SUBTARGET}/${SDK_NAME}.tar.xz" \
SDK_HOME="/opt/${SDK_NAME}"

ENV \
MAKEFLAGS="--directory=${SDK_HOME} MM_SHM_MAXSEGSIZE=67108864"

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
# download and enter SDK
&& apk --no-cache add make bash gawk perl python3 file gcc g++ musl-dev ncurses-dev bzip2 git rsync \
&& echo "Downloading ${SDK_URL} ..." && mkdir -p "${SDK_HOME%/*}" && wget -qO - "${SDK_URL}" | tar xJ -C "${SDK_HOME%/*}" && echo "OpenWrt SDK downloaded and extracted" \
&& cd "${SDK_HOME}" \
# configure SDK
&& /reconfigure.sh \
# download packages
&& ./scripts/feeds update -a \
# clean up
&& apk del git \
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*
